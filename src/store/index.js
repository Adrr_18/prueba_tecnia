import { createStore } from 'vuex'
import axios from 'axios'
const url = 'https://pokeapi.co/api/v2/'
export default createStore({
    state: {
        listPokemons: [],

    },
    mutations: {
        setListPokemon(state, payload) {
            state.listPokemons = payload
        }
    },
    actions: {
        async searchPokemons({ commit }) {
            var config = {
                method: 'get',
                url: `${url}pokemon`,
                headers: {}
            };

            await axios(config)
                .then(function(response) {
                    console.log(JSON.stringify(response.data.results));
                    commit('setListPokemon', response.data.results)

                })
                .catch(function(error) {
                    console.log(error);
                });
        },

    },
    modules: {}
})