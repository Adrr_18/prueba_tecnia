import { createApp } from 'vue';
import App from './App.vue';
import router from './router'
import store from './store'

import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import Menubar from 'primevue/menubar';
import DataTable from 'primevue/datatable'
import Column from 'primevue/column'
import Fieldset from 'primevue/fieldset';
import Accordion from 'primevue/accordion';
import AccordionTab from 'primevue/accordiontab';

import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css'
import 'primeicons/primeicons.css'

const app = createApp(App).use(store).use(router);
app.use(PrimeVue);

app.component('inputText', InputText);
app.component('Button', Button);
app.component('Menubar', Menubar);
app.component('DataTable', DataTable);
app.component('Column', Column);
app.component('Fieldset', Fieldset);
app.component('Accordion', Accordion);
app.component('AccordionTab', AccordionTab);



app.mount('#app')
app.use(router).use(store)